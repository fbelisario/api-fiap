const chai = require('chai');
const subSet = require('chai-subset');

const Cavaleiro = require('../lib/controller/Cavaleiro');

chai.use(subSet);

const cavaleiroSchema = {
    nome:   nome => nome,
    descricao: descricao => descricao,
    ataques: ataques => ataques
}

describe('Teste UNITARIO - classe Cavaleiro', function(){
    let cavaleiro = new Cavaleiro;

    it('Set Nome',function(){
        let cavaleiroNome = cavaleiro.nome = 'Seiya de Pegaus';        
        chai.expect(cavaleiroNome).to.be.a('String')
    })

    it('Set Descricao', function(){
        let cavaleiroDescricao = cavaleiro.descricao = 'Seiya é o Cavaleiro de Bronze da constelação de Pegasus. Quando criança, Seiya foi separado de sua irmã, Seika, para fazer o treinamento para Cavaleiro no Santuário de Athena, na Grécia. Graças aos métodos rígidos e disciplinares de sua mentora Marin de Águia, Seiya foi capaz de cultivar um enorme potencial e conquistar a Armadura de Pégaso. Motivado pelo desejo de reencontrar sua irmã, Seiya depois descobre seu destino como um verdadeiro Cavaleiro de Athena, renascendo sempre que a deusa reencarna, para ajudá-la na batalha contra o mal que consome a Terra. Guerreiro de imenso poder, Seiya alcança a vitória em batalhas aparentemente impossíveis e consegue derrotar até mesmo os deuses Poseidon e Hades.';
        chai.expect(cavaleiroDescricao).to.be.a('String')
    });

    it('Set Ataques',function(){
        let cavaleiroAtaques = cavaleiro.ataques = [
            "Pegasus Ryūsei Ken - Meteoros de Pégaso",
            "Pegasus Suisei Ken - Cometa de Pégaso",
            "Pegasus Rolling Crush - Turbilhão de Pégaso"
        ];

        chai.expect(cavaleiroAtaques).to.have.lengthOf(3);
    })

    it('Get infoCavaleiro', function(){
        cavaleiro.nome =  'Seiya de Pegaus' ;
        cavaleiro.descricao = 'Seiya é o Cavaleiro de Bronze da constelação de Pegasus. Quando criança, Seiya foi separado de sua irmã, Seika, para fazer o treinamento para Cavaleiro no Santuário de Athena, na Grécia. Graças aos métodos rígidos e disciplinares de sua mentora Marin de Águia, Seiya foi capaz de cultivar um enorme potencial e conquistar a Armadura de Pégaso. Motivado pelo desejo de reencontrar sua irmã, Seiya depois descobre seu destino como um verdadeiro Cavaleiro de Athena, renascendo sempre que a deusa reencarna, para ajudá-la na batalha contra o mal que consome a Terra. Guerreiro de imenso poder, Seiya alcança a vitória em batalhas aparentemente impossíveis e consegue derrotar até mesmo os deuses Poseidon e Hades.';
        cavaleiro.ataques = [
            "Pegasus Ryūsei Ken - Meteoros de Pégaso",
            "Pegasus Suisei Ken - Cometa de Pégaso",
            "Pegasus Rolling Crush - Turbilhão de Pégaso"
        ];

        novoCavaleiro = cavaleiro.infoCavaleiro;

        chai.expect(novoCavaleiro).to.containSubset(cavaleiroSchema);
    })
})