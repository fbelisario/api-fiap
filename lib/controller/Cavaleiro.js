
class Cavaleiro {
    constructor(){
        this._nome = null;
        this._descricao = null;
        this._ataques = [];
    }

    set nome(nome){
        this._nome = nome;
        return this._nome;
    }

    set descricao(descricao){
        this._descricao = descricao;
        return this._descricao;
    }

    set ataques(ataques){
        this._ataques = ataques;
        return this._ataques;
    }

    get infoCavaleiro(){
        let cavaleiro = {
            nome: this._nome,
            descricao: this._descricao,
            ataques: this._ataques
        }

        return cavaleiro;
    }

    cadastrarCavaleiro(nome,descricao,ataques){

    }
}

module.exports = Cavaleiro;